# The R Package trafo for Transforming Linear Regression Models
# y를 변환해서 LINE 가정을 충족하도록 도와줌...

library(trafo)
library(Ecdat)
data(University)

linMod <- lm(nassets ~ stfees, data = University)
assumptions(linMod)

linMod_trafo <- trafo_lm(linMod)
diagnostics(linMod_trafo)
plot(linMod_trafo)


linMod_trafo2 <- trafo_lm(object = linMod, 
                          trafo = "logshiftopt",
                          method = "skew")
diagnostics(linMod_trafo2)
plot(linMod_trafo2)


## 3.3 Comparing two transformed models
boxcox_uni <- boxcox(linMod)
log_uni <- logtrafo(linMod)

head(as.data.frame(boxcox_uni))
linMod_comp <- 
  trafo_compare(object = linMod,
                trafos = list(boxcox_uni, log_uni))
diagnostics(linMod_comp)

## 4. Customized transformation
glog_trafo <- function(y) {
  yt <- log(y + sqrt(y^2 + 1))
  return(y = yt) }

glog_std <- function(y) {
  zt <- log(y + sqrt(y^2 + 1)) * sqrt(geometric.mean(1 + y^2))
  return(zt = zt)
}

linMod_custom <- 
  trafo_lm(linMod, trafo = 'custom_wo',
           custom_trafo = list(glog_trafo = glog_trafo, 
                               glog_std = glog_std))

