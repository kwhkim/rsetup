# test_Rcpp_RcppArmadillo.R
library(Rcpp)
library(RcppArmadillo)

'
You open New C++ File in R Studio.

And change "# include <Rcpp.h>" to

"
# include <RcppArmadillo.h>
// [[ Rcpp :: depends ( RcppArmadillo )]]
"
'

sourceCpp('matMultiply.cpp')
#
#matMultiply.cpp:1:27: fatal error: RcppArmadillo.h: No such file or directory
##include <RcppArmadillo.h>
#^
#  compilation terminated.

#https://stackoverflow.com/questions/23741689/setting-up-rcpp-armadillo-in-windows-with-rstudio

# !!! // [[Rcpp::depends(RcppArmadillo)]] !!!
#  // [[ Rcpp :: depends ( RcppArmadillo )]] DOES NOT WORK!!!


cppFunction("arma::mat AB (arma::mat A, arma::mat B) {
  return A * B  ;
}
", 
depends="RcppArmadillo")



A = matrix(rnorm(400*500), 400, 500)
B = matrix(rnorm(500*300), 500, 300)

benchmark(cpp = { AB(A,B)},
          R = {A %*% B})
