save0 <- save
save <- function(file = file, ...) {
  if (file.exists(file)) { 
    stop("file exists")
  } else { 
    save0(file=file, ...) 
  }}
