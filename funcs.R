x_label_date = function(cycle) {
  if (cycle == '10 years') {
    return(scale_x_date(date_breaks = '10 years', date_labels = '%Y'))
  } else if (cycle == '1 year') {
    return(scale_x_date(date_breaks = '1 year', date_labels = '%Y'))
  } else if (cycle == '1 month') {
    return(scale_x_date(date_breaks = '1 month', date_labels = '%Y-%m'))
  } else if (cycle == '1 week') {
    return(scale_x_date(date_breaks = '1 week', date_labels = '%Y-%m-%d'))
  } else if (cycle == '1 day') {
    return(scale_x_date(date_breaks = '1 day', date_labels = '%Y-%m-%d'))
  } else if (cycle == '1 hour') {
    return(scale_x_datetime(date_breaks = '1 hour', date_labels = '%m-%d %H:%M'))
  } else {
    message("consider `scale_x_date(date_breaks = '1 day', date_labels='%d')` with %Y, %y, %m, %d, %H, %M, or %S")
    error("cycle unknown. try x_label_date('1 day') and modify it yourself")
  }
}

x_label_datetime = function(cycle) {
  if (cycle == '1 day') {
    return(scale_x_datetime(date_break='1 day', date_labels = '%y-%m-%d'))
  } else if (cycle == '1 week') {
    return(scale_x_datetime(date_breaks = '1 week', date_labels = '%y-%m-%d'))
  } else if (cycle == '1 month') {
    return(scale_x_datetime(date_breaks = '1 month', date_labels = '%y-%m-%d')) 
  } 
   # else if (cycle == 'hour only') {
  #  return(scale_x_datetime(datetime_breaks = '1 hour', date_labels = '%H:%M')) }  
}

x_label_vertical = function() {
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))
}

getmode <- function(v) {
  uniqv <- unique(v)
  uniqv[which.max(tabulate(match(v, uniqv)))]
}

range2 = function(x) {
  max(x) -min(x)
}


env_checkprocess = new.env()
# start = function(env, program, process) {
#   #stopifnot(!(missing(program) & missing(process))) # Error when both program or proess is empty
#   #row = data.frame(program = ifelse(missing(program), "", program),
#   #           process = ifelse(missing(process), "", process))
#   
#   env$start = Sys.time()
# }
env_checkprocess = new.env()

start_process = function(env=env_checkprocess) {
  env$start = Sys.time()
}
end_process = function(env=env_checkprocess) {
  cat('Mins Elapsed = ', sprintf("%5.1f", as.numeric(difftime(Sys.time(), env$start, units='mins'))))
  env$start = NULL
}

start_process()
end_process()

legend_col_alpha1 = function() {
  guides(colour = guide_legend(override.aes=list(alpha=1)))
}

table = function(..., useNA = 'ifany') {
  base::table(..., useNA = useNA)
}

time_to_min = function(time) {
  stopifnot(class(time) == 'character')
  stopifnot(all(substr(time, 3,3) == ':'))
  #stopifnot(all(substr(time, 6,6) == ':'))
  h = as.numeric(substr(time, 1, 2))
  m = as.numeric(substr(time, 4, 5))
  #s = as.numeric(substr(time, 7, 8))
  #return(h*3600 + m*60 + s)
  return(h*60 + m)
}

time_to_sec = function(time) {
  stopifnot(class(time) == 'character')
  stopifnot(all(substr(time, 3,3) == ':'))
  stopifnot(all(substr(time, 6,6) == ':'))
  h = as.numeric(substr(time, 1, 2))
  m = as.numeric(substr(time, 4, 5))
  s = as.numeric(substr(time, 7, 8))
  return(h*3600 + m*60 + s)
  #return(h*60 + m)
}

Unique = function(x) {
  res <- unique(x)
  stopifnot(length(res)==1)
  res
}

is.holiday = function(date) {
  require(lubridate)
  stopifnot(all(class(date) == "Date"))
  
  stopifnot(all(is.na(year(date)) | year(date)==2019 | year(date)==2020))
  
  return(ifelse(date %in% 
                  as.Date(c(
                    '2019-01-01', '2019-02-04', '2019-02-05', '2019-02-06', 
                    '2019-03-01', '2019-05-05', '2019-05-12', '2019-06-06',
                    '2019-08-15', '2019-09-12', '2019-09-13', '2019-09-14',
                    '2019-10-03', '2019-10-09', '2019-12-25',
                    '2020-01-01', '2020-01-24', '2020-01-25', '2020-01-26', 
                    '2020-01-27', '2020-03-01', '2020-04-15', '2020-04-30',
                    '2020-05-01', '2020-05-05','2020-06-06',
                    '2020-08-15', '2020-08-17', '2020-09-30', '2020-10-01', '2020-10-02',
                    '2020-10-03', '2020-10-09', '2020-12-25')),
                TRUE, FALSE))
}

inrange = function(x, range) {
  range = sort(range)
  a = x >= range[1]
  b = x <= range[2]
  a & b
}

# 만약 손해라면 다시 시작
cumsum_positive = function(x) {
  stopifnot(is.vector(x))
  y = rep(NA, length(x))
  cum  = 0
  for (i in 1:length(x)) {
    if (!is.na(x[i]) & x[i]<0) {
      y[i] = 0
      cum=0
    } else if (!is.na(x[i]) & x[i] >= 0) {
      cum = x[i] + cum
      y[i] = cum
    } else {
      y[i] = 0
      cum = 0
    }
  }
  return(y)
}



is.weekend = function(date) {
  return(lubridate::wday(date) %in% c(1,7)) # Sunday or Saturday)
}

cumsum = function(x,na.ignore = TRUE) {
  if (na.ignore==TRUE) {
    x[is.na(x)] = 0
  }
  base::cumsum(x)
}

catv = function(v) {
  stopifnot(is.vector(v))
  cat(paste0(list.files(path='.', pattern = '[.]R$'), '\n', collapse=''))
}

pmean = function(..., na.rm=FALSE) {
    mat <- matrix(unlist(list(...)), nrow=length(..1))
    apply(mat, 1,mean, na.rm=na.rm)
  }
  
min_overhalf = function(x) {
  if (sum(is.na(x)) > length(x)/2) {
    return(NA)
  } else {
    return(min(x, na.rm=TRUE))
  }
}


cat('*** The following functions are defined\n')
cat('x_label_date("10 years"); #1 year, 1 month, 1 week, 1 day\n')
cat('x_label_datetime("1 day")\n')
cat('x_label_vertical(); \n')
cat('legend_col_alpha1(); \n')
cat('getmode(v)\n')
cat('range2(v)\n')
cat('catv(v) for print(vector) withouth [1] or [2]\n')
cat('table(..., useNA = "ifany")\n')
cat('time_to_min() : converts character 00:00 to minute\n')
cat('is.holiday() : check if Korean Holiday for year 2019, 2020\n')
cat('is.weekend() : check if Saturday or Sunday\n')
cat('inrange(x, rang) : check if rang[1] < x and x < rang[2]\n')
cat('cumsum(x, na.ignore=TRUE) : base::cumsum with replacing NA with 0\n')
cat('cumsum_positive(x, na.ignore=TRUE) : cumsum when hit negative, cumsum goes back to 0. no call to cumsum()\n')
cat('pmean(v1, v2, v3, ..., na.rm=TRUE) : pmean something like pmax, pmin')
cat('min_overhalf(x) : if x has more than half of the total number of members NA, if not just min of the non missing values\n')
cat('start_process()\n')
cat('end_process()\n')

cat('closing device...\n')
tryCatch({dev.off()},
         error=function(e) { print(e)})
tryCatch({dev.off()},
         error=function(e) { print(e)})
tryCatch({dev.off()},
         error=function(e) { print(e)})