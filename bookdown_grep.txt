# 새로운 prefix를 code label로 넣기 (아래 예 mm)
^```[{]r ([^ },]*)
```{r mm\1

# \ref에 새로운 label 적용하기  
\\ref{(tab|fig):
\\ref{\1:mm
        
# # 늘리기 
^([#]+) 
\1# 