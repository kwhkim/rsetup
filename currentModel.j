
# Simple linear regression with endogeneity

model {
    # model
    for (i in 1:N) {
        meany[i] <- beta0 + xx[i]*beta1
        meane[i] <- rho*(xx[i]-meanxx)
        meanype[i] <- meany[i] + meane[i]
        y[i] ~ dnorm(meanype[i], tau)
    }

    # priors
    rho <- covXE/varX
    tau <- pow(sigma2, -2)
    varE <- pow(rho, 2)*varX + pow(sigma2,2)

    covXE ~ dnorm(5, 1)
    beta0 ~ dnorm(0, 0.01)
    beta1 ~ dnorm(0, 0.01)
    sigma2 ~ dunif(0,100)
}
